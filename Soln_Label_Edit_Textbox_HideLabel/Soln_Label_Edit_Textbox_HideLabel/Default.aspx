﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="Soln_Label_Edit_Textbox_HideLabel.Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="css/StyleSheet.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
      
    <div style="text-align:center">
      <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>

                <asp:Label ID="lblfirstname" runat="server" Text="Mangal Singh" />
                
                <asp:Label ID="lbllastname" runat="server" Text="Gill" /><br />
           
                <asp:Label ID="lblage" runat="server"  Text="21"/><br />
                
                <asp:Button ID="btnedit" runat="server" Text="Edit" CssClass="btn btn2" OnClick="btnedit_Click" />

                <asp:Button ID="btnsubmit" runat="server" Text="Submit" CssClass="btn btn2" OnClick="btnsubmit_Click" /><br />

                <asp:TextBox ID="txtfirstname" runat="server" Visible="false" /><br />

                 <asp:TextBox ID="txtlastname" runat="server" Visible="false" /><br />

                 <asp:TextBox ID="txtage" runat="server" Visible="false" /><br />

            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    </form>
</body>
</html>
