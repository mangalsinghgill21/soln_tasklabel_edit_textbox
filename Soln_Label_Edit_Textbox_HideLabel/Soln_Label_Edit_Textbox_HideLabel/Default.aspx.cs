﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Soln_Label_Edit_Textbox_HideLabel
{
    public partial class Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnedit_Click(object sender, EventArgs e)
        {
            lblfirstname.Visible = false;
            lbllastname.Visible = false;
            lblage.Visible = false;

            txtfirstname.Text =Convert.ToString(lblfirstname);
            txtlastname.Text =Convert.ToString (lbllastname);
            txtage.Text =Convert.ToString(lblage);

            txtfirstname.Visible = true;
            txtlastname.Visible = true;
            txtage.Visible = true;
        }

        protected void btnsubmit_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/Default.aspx");
        }
    }
}